echo "system updating..."
{
    apt-get update && apt-get upgrade
} || {
    echo "Aravis installation failed"
    exit
}
echo "system upgraded"
{
    apt-get install autoconf intltool libxml2-dev python-gobject-dev gobject-introspection\
	    gtk-doc-tools libgstreamer0.10-dev python-gst0.10-dev
    apt-get install  libgirepository1.0-dev libgtk-3-dev libnotify-dev libgstreamer1.0\
	    libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-bad libgirepository1.0-dev
} || {
    echo "Dependencies installation failed"
    exit
}
echo "aravis configuring..."
{
    ./autogen.sh 
    ./configure --enable-viewer --enable-introspection=yes  --enable-gtk-doc --enable-gst-plugin-0.10\
		--enable-gst-plugin --disable-silent-rules --enable-usb  --enable-fast-heartbeat
} || {
    echo "Aravis configuration failed"
    exit
}
{
    make
} || {
    echo "Aravis make failed"
    exit
}
echo "installiang make..."
{
    make install
} || {
    echo "Aravis installation failed"
    exit
}
echo "aravis installed"
{
    cp /etc/profile /etc/profile.bkup
    chmod 777 /etc/profile
    echo export GI_TYPELIB_PATH=\$GI_TYPELIB_PATH:$PWD/src >> /etc/profile
    echo export LD_LIBRARY_PATH=$PWD/src/.libs >> /etc/profile
    chmod 644 /etc/profile
    source /etc/profile
} || {
    echo "Failed to set path"
    echo "Please set path manually"
    exit
}
echo "installation completed."
