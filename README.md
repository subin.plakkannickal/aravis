installation

	sudo chmod 777 installer.sh
	sudo ./installer.sh

For USB 3.0

        copy aravis/aravis.rules to /etc/udev/rules.d/aravis.rules
        
        If you want to add an entry with the vendor of your camera,
	the output of lsusb command will give you the vendor id,
	which is the first 4 digits of the ID field.
        